const fs = require('fs')

const http = require('http')
const ws = require('ws')

const chalk = require('chalk')

console.log(chalk.blue('Starting websockets server on port 8081'))
const wss = new ws.Server({ port: 8081 })

function broadcast (type, data, exclude) {
  wss.clients.forEach(function (ws) {
    if (exclude && exclude.indexOf(ws._socket.remoteAddress) >= 0) {
      return
    }
    var _data = {
      type: type,
      ts: (new Date()).getTime(),
      data: data
    }
    ws.send(JSON.stringify(_data), function (error) {
      if (error) {
        console.log(chalk.red(`error: ${error}`))
      }
    })
  })
}

wss.on('connection', function connection(ws, req) {
  const ip = req.connection.remoteAddress
  console.log(chalk.yellow('websockets connection from ' + ip))

  ws.isAlive = true
  ws.on('pong', function () {
    ws.isAlive = true
  })

  broadcast('updateclients', { clients: wss.clients.size })

  ws.on('close', function () {
    broadcast('updateclients', { clients: wss.clients.size })
    console.log(chalk.white(`client ${ip} disconnected`))
  })

  ws.on('message', function (msg) {
    var data = {
      type: 'message',
      ts: new Date(),
      data: `Got your message "${msg}"!`
    }

    broadcast('usrmessage', { usr: ip, msg: msg }, [ip])

    ws.send(JSON.stringify(data), function (error) {
      if (error) {
        console.log(chalk.red(`error: ${error}`))
      }
    })

    console.log(chalk.magenta(`got message from client ${ip}: ${msg}`))
  })

  ws.on('error', function (error) {
    console.log(chalk.white(`error with client ${ip}: ${error}`))
  })
})

setInterval(function () {
  wss.clients.forEach(function (ws) {
    if (ws.isAlive === false) {
      console.log(chalk.red('no pong response, terminating connection'))
      return ws.terminate()
    }

    ws.isAlive = false
    ws.ping(function () {})
  })
}, 30000)

setInterval(function () {
  var rnd = Math.random() * 1000 >> 0
  broadcast('broadcastevent', { number: rnd })
}, 3000)

console.log(chalk.blue('Starting http server on port 8080'))
http.createServer(function (req, res) {

  var r = '/index.html'

  switch (req.url) {
    case '/':
      break
    case '/vue.min.js': 
    case '/bootstrap.min.css':
    case '/bootstrap.min.css.map':
      r = req.url
      break
    case '/favicon.ico':
      res.writeHead(200, { 'Content-Type': 'image/x-icon' })
      res.end('')
      return
    default:
      res.writeHead(301, { 'Location': '/' })
      res.end('')
      return
  }

  fs.readFile('.' + r, function(error, content) {
    if (error) {
      console.log(chalk.red(`${r}... 500`))
      res.writeHead(500)
      res.end('Server has encountered an error.\n')
    }
    else {
      console.log(chalk.green(`${r} ... 200`))
      switch (r.split('.').pop()) {
        case 'js':
          res.writeHead(200, { 'Content-Type': 'text/javascript' })
          break
        case 'css':
          res.writeHead(200, { 'Content-Type': 'text/css' })
          break
        case 'html':
          res.writeHead(200, { 'Content-Type': 'text/html' })
          break
      }
      res.end(content, 'utf-8')
    }
  })
}).listen(8080)
